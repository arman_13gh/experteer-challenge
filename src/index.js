import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import HeaderImage from './components/HeaderImage';
import SingleJob from './pages/SingleJob';
import { Provider } from 'react-redux';
import { store } from './store'
import ScrollToTop from './components/ScrollToTop'

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <ScrollToTop />
      <HeaderImage />
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/job/:id/:query/:location" element={<SingleJob />} />
        <Route path="/job/:id/:query" element={<SingleJob />} />
        <Route
          path="*"
          element={
            <main style={{ padding: "5rem" }}>
              <p>Wrong Page !</p>
            </main>
          }
        />
      </Routes>
    </Provider>
  </BrowserRouter>
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
