import { React} from 'react';
import './App.scss';
import Loader from './components/Loader';
import SearchField from './components/SearchField';
import {useSelector} from 'react-redux';
import JobsList from './components/JobsList';

function App() {
  const loadingState = useSelector(state=>state.loadingState)

  return (
    <div className="App">
      <SearchField/>
      {loadingState ? <Loader /> :
        <JobsList />
      }
    </div>
  );
}

export default App;
