import './HeaderImage.scss';

function HeaderImage() {
    return (
        <div className='header-image-wrapper'>
            <div className="header-image"></div>
            <div className="header-image-gradient"></div>
        </div>
    );
}

export default HeaderImage;
