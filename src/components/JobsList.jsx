import JobCard from '../components/JobCard';
import { Col, Container, Row } from 'react-bootstrap';
import { Link } from "react-router-dom";
import {useSelector,useDispatch} from 'react-redux';

function JobsList() {
    const jobsState = useSelector(state=>state.jobsData)
    const queryState = useSelector(state=>state.searchData)
    const dispatch = useDispatch();

    const saveJob = (job) => {
        dispatch({ type: "saveSingleJob", payload: job })
    }

    return (
        <Container>
            {
                jobsState !== [] && jobsState.length ? jobsState.slice(0, 10).map((job) => (
                    <Row key={job.Guid} className=" justify-content-center mt-5">
                        <Col md={12} sm={12} xs={12} lg={7} >
                            <div onClick={() => {saveJob(job)}}>
                            <Link to={`/job/${job.Guid}/${queryState.query}/${queryState.location}`}>
                                <JobCard job={job} />
                            </Link>
                            </div>
                        </Col>
                    </Row>
                )) : <h1 className='text-center mt-5'>no data founded</h1>
            }
        </Container>
    );
}

export default JobsList;
