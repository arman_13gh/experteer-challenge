import { useState } from "react";
import './AutoComplete.scss'

const AutoComplete = ({ suggestions ,getLocationState,setLocationState}) => {
    const [filteredSuggestions, setFilteredSuggestions] = useState([]);
    const [activeSuggestionIndex, setActiveSuggestionIndex] = useState(0);
    const [showSuggestions, setShowSuggestions] = useState(false);
    const [input, setInput] = useState("");
    
    const onChange = (e) => {
      // Filter suggestions that don't contain the user's input
        const userInput = e.target.value;
        const unLinked = suggestions.filter(
          (suggestion) =>
            suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
        );
        setInput(e.target.value);
        setFilteredSuggestions(unLinked);
        setActiveSuggestionIndex(0);
        setShowSuggestions(true);
        getLocationState(e.target.value);
      };
      const onClick = (e) => {
        setFilteredSuggestions([]);
        setInput(e.target.innerText);
        setActiveSuggestionIndex(0);
        setShowSuggestions(false);
        getLocationState(e.target.innerText);
      };
      const SugList = () => {
          return filteredSuggestions.length ? (
            <ul className="suggestions">
              {filteredSuggestions.map((suggestion, index) => {
                // Flag the active suggestion with class
                let className;
                if (index === activeSuggestionIndex) {
                  className = "suggestion-active";
                }
                return (
                  <li className={className} key={suggestion} onClick={onClick}>
                    {suggestion}
                  </li>
                );
              })}
            </ul>
          ) : (
            <div className="no-suggestions">
              <em>No suggestions</em>
            </div>
          );
        };
    return (
        <>
            <input
                type="text"
                onChange={onChange}
                value={setLocationState || ""}
                placeholder="Location"
                className="input-original w-100"
            />
            {showSuggestions && input && <SugList />}
        </>
    );
};
export default AutoComplete;