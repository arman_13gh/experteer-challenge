import './JobCard.scss';
import './Badge.scss';
import locationIcon from '../assets/icons/location-icon.svg';
import { useState, useEffect } from 'react';

// use a safe parser instead of using dangerousInnerHtml that has the chance of cross-site scripting attack
import parse from 'html-react-parser';


function JobCard({ job }) {

    const [applied, setApplied] = useState(false)
    useEffect(() => {
        var arrayIds = JSON.parse(window.localStorage.getItem("appliedJobs"));
        if (arrayIds !== [] && arrayIds !== null) {
            if (!Array.isArray(arrayIds)) {
                arrayIds = Array.from(arrayIds);
            }
            const index = arrayIds.findIndex(object => object === job.Guid);
            console.log(index);
            if (index === -1) {
                setApplied(false)
            } else {
                setApplied(true)
            }
        }
    }, []);

    return (
        <div className='job-card-wrapper text-left'>
            <div className='job-card-title-wrapper'>
                <div className='job-card-title-left-side'>
                    <h5>{job.Title}</h5>
                    {applied && <div className='badge-applied-wrapper'>
                        <div className='badge-applied-fake-wrapper'>
                            <div className='badge-applied'>
                                <p>APPLIED</p>
                            </div>
                        </div>
                    </div>
                    }
                </div>
                <div className="badge-wrapper badge-wrapper-full-time">
                    <div className="badge-fake-wrapper">
                        <div className='badge  badge-full-time'>
                            full-time
                        </div>
                    </div>
                </div>
            </div>
            <h5 className='mb-3'>{job.Company}</h5>
            <div className='job-card-description'>
                {parse(job.Description)}
                <div class="job-card-gradient-end"></div>
            </div>
            <div className='mb-4'>
                <img className='location-icon' src={locationIcon} alt="location icon" />
                <p className='location-text'>{job.Location}</p>
            </div>
            <p>{job.Published}</p>

        </div>
    );
}

export default JobCard;
