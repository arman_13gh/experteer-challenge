import './Loader.scss';
import loader from '../assets/icons/loader.svg';
function Loader() {
    return (
        <div className='loading-container'>
        <p className='wait-result'>Your results will apear here ...</p>
        <div className='loading-wrapper'>
            <img src={loader} alt="loader" />
        </div>
        </div>
    );
}

export default Loader;
