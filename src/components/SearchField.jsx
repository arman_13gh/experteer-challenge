import './SearchField.scss';
import { useState, useEffect, React } from 'react';
import JobService from '../services/GetJobs';
import { useDispatch } from 'react-redux';
import AutoComplete from './AutoComplete';
import { useNavigate, useLocation } from 'react-router-dom';
function SearchField() {

    const [formInput, updateFormInput] = useState({ query: "", location: "" });
    const { search } = window.location;
    let navigate = useNavigate();
    const dispatch = useDispatch();
    const location = useLocation();
    // when ever user navigates to this page check if we have url params and fetch data
    useEffect(() => {
        setDataFromUrlParams();
    }, [location]);

    const saveJob = (data, search) => {
        dispatch({ type: "saveJob", payload: { data, search: { query: search.query, location: search.location } } })
    }
    const setLoadingState = (boolean) => {
        dispatch({ type: "loadingState", payload: boolean })
    }

    async function triggerSearch(e) {
        if (formInput.query) {
            navigate(`${'?query=' + formInput.query + '&location=' + formInput.location}`);
            e.preventDefault();
        }
    }

    async function searchJobs(query, location) {
        setLoadingState(true)
        JobService.searchJobs(query, location).then((res) => {
            let data = res.data;
            let status = res.status;
            switch (status) {
                case 201:
                    checkJobsStatus(data, { query: query, location: location });
                    break;
                case 200:
                    saveJob(data, { query: query, location: location });
                    setLoadingState(false);
                    break;
                case 404:
                    setLoadingState(false)
                    break;
                default:
                    return
            }
        })
            .catch((error) => {
                console.log(error)
                setLoadingState(false)
            });
    }

    async function checkJobsStatus(uuid, object) {
        JobService.checkJobsStatus(uuid).then((res) => {
            let status = res.status;
            switch (status) {
                case 200:
                    getJobs(uuid, object);
                    break;
                case 202:
                    setTimeout(function () { checkJobsStatus(uuid, object); }, 2000);
                    break;
                case 404:
                    setLoadingState(false)
                    break;
                default:
                    return
            }
        })
            .catch((error) => {
                console.log(error)
                setLoadingState(false)
            });
    }

    async function getJobs(uuid, object) {
        JobService.getJobsList(uuid).then((res) => {
            saveJob(res, { query: object.query, location: object.location });
            setLoadingState(false)
        })
            .catch((error) => {
                console.log(error)
                setLoadingState(false)
            });
    }

    const setDataFromUrlParams = () => {
        updateFormInput({ query: new URLSearchParams(search).get('query'), location: new URLSearchParams(search).get('location') });
        searchJobs(new URLSearchParams(search).get('query'), new URLSearchParams(search).get('location'));
    }

    const getLocationState = (string) => {
        updateFormInput({ ...formInput, location: string })
    };
    return (
        <div className='container'>
            <h1 className='text-welcome mb-5 text-center'>Welcome to DevJobs</h1>
            <form action='/' method="GET" onSubmit={triggerSearch} autoComplete='off'>

                <label htmlFor="header-search">
                    <span className="visually-hidden">Search blog posts</span>
                </label>
                <div className='header-form-wrapper mt-5 row'>
                    <div className='col-12 col-lg-3 col-md-5 my-2 my-md-0'>
                        <AutoComplete
                            getLocationState={getLocationState}
                            setLocationState={formInput.location}
                            suggestions={[
                                "Berlin",
                                "Banco",
                                "Brazil",
                                "Munich",
                                "London",
                                "New york",
                                "Tehran",
                                "Jaws"
                            ]}
                        />
                    </div>
                    <div className='col-12 col-lg-3 col-md-5 my-2 my-md-0'>
                        <input value={formInput.query || ""} name="query" id="header-search" type="text" onChange={e => updateFormInput({ ...formInput, query: e.target.value })} placeholder='find your dream job now' className='input-original  w-100' />
                    </div>
                    <div className='col-12 col-lg-1 col-md-2 my-2 my-md-0 text-left'>
                        <button type='submit' className='btn btn-original box-shadow-small'>Search</button>
                    </div>
                </div>
            </form>
        </div >
    );
}

export default SearchField;


