import axios from 'axios'
import configData from '../config.json'

//  using HTTP instead of websocket because updating the jobs detail isnt something to be happend every second
//  HTTP is highly cacheable resource
//  HTTP is designed around the request-response messaging pattern, which means it has extensive support for error scenarios

const JobService = {
    searchJobs: async function (query, location) {
        if (query) {
            if(!location || location === null || location === "null") location="Munich";
            try {
                const response = await axios.post(configData.BASE_URL + "/search", null, { params: { query, location } })
                return response
            } catch (error) {
                throw error;
            }

        }
    },
    checkJobsStatus: async function (uuid) {
        try {
            const response = await axios.get(configData.BASE_URL + "/status/" + uuid)
            return response
        } catch (error) {
            throw error;
        }
    },
    getJobsList: async function (uuid) {
        try {
            const response = await axios.get(configData.BASE_URL + "/result/" + uuid)
            return response.data
        } catch (error) {
            throw error;
        }
    }
}
export default JobService