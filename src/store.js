import { createStore } from 'redux';

export const initialState = {
    jobsData: {},
    searchData: {},
    singleJobData: {
        Title:"",
        Guid:"",
        Description:"",
        Published:"",
        Company:"",
        Location:""
    },
    loadingState: false
}

function reducer(state = initialState, action) {
    switch (action.type) {
        case "saveJob":
            return { ...state, jobsData: action.payload.data, searchData: action.payload.search };
        case "saveSingleJob":
            return { ...state, singleJobData: action.payload };
        case "loadingState":
            return { ...state, loadingState: action.payload };
        default:
            return state;
    }
}


export const store = createStore(reducer);