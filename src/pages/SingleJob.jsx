import { Container, Row, Col } from "react-bootstrap";
import { useState, useEffect } from 'react';
import { useParams, useNavigate, useLocation } from "react-router-dom";
import backIcon from '../assets/icons/back-arrow.svg';
import locationIcon from '../assets/icons/location-icon.svg';
import JobService from '../services/GetJobs';
import '../components/Badge.scss';
import './SingleJob.scss';
import {useSelector,useDispatch} from 'react-redux';
// use a safe parser instead of using dangerousInnerHtml that has the chance of cross-site scripting attack
import parse from 'html-react-parser';

function SingleJob() {
    const [applied, setApplied] = useState(false)
    const jobState = useSelector(state=>state.singleJobData)
    const dispatch = useDispatch();

    const saveSingleJob = (data) => {
        dispatch({ type: "saveSingleJob", payload: data })
    }
    let params = useParams();
    const navigate = useNavigate();
    const location = useLocation();

    // local storage for applied jobs if it dosent exist create one (thats just for the first time)
    var arrayIds = [];
    if (window.localStorage.getItem("appliedJobs")) {
        arrayIds = JSON.parse(window.localStorage.getItem("appliedJobs"));
    } else {
        arrayIds[0] = 'applied jobs list'
    }

    useEffect(() => {
        triggerSearch(params.query, params.location)
        checkApply();
    }, [location]);

    async function triggerSearch(query, location) {
        JobService.searchJobs(query, location).then((res) => {
            let data = res.data;
            let status = res.status;
            switch (status) {
                case 201:
                    checkJobsStatus(data);
                    break;
                case 200:
                    const jobData = data.find(x => x.Guid === params.id)
                    saveSingleJob(jobData);
                    break;
                case 404:
                    break;
                default:
                    return
            }
        })
            .catch((error) => {
                console.log(error)
            });
    }

    async function checkJobsStatus(uuid) {
        JobService.checkJobsStatus(uuid).then((res) => {
            let status = res.status;
            switch (status) {
                case 200:
                    getJobs(uuid);
                    break;
                case 202:
                    setTimeout(function () { checkJobsStatus(uuid); }, 2000);
                    break;
                case 404:
                    break;
                default:
                    return
            }
        })
            .catch((error) => {
                console.log(error)
            });
    }

    async function getJobs(uuid) {
        JobService.getJobsList(uuid).then((res) => {
            const jobData = res.find(x => x.Guid === params.id)
            saveSingleJob(jobData);
        })
            .catch((error) => {
                console.log(error)
            });
    }
    // local storage only gets string so i use 'stringify' and 'parse' because i need array of applied ids
    const Apply = () => {
        // if id already exist dont add it
        const index = arrayIds.findIndex(object => object === params.id);
        if (index === -1) {
            arrayIds.push(params.id);
            setApplied(true)
        } else {
            setApplied(true)
        }
        window.localStorage.setItem("appliedJobs", JSON.stringify(arrayIds));
    }
    const checkApply = () =>  {
        const index = arrayIds.findIndex(object => object === params.id);
        if (index === -1) {
            setApplied(false)
        } else {
            setApplied(true)
        }
    }

    return (
        <Container>
            <Row className="justify-content-center single-job-container">
                <Col className="single-job-wrapper col-md-8 px-1 px-md-5 ">
                    <Row>
                        <Col>
                            <div className="cursor-pointer" onClick={() => navigate(-1)}>
                                <img alt="navigate back" height={40} width={40} src={backIcon} />
                            </div>
                        </Col>
                    </Row>
                    {jobState && 
                    <Row className="px-5">
                        <Col md={9}>
                            <h3>
                                {jobState.Title}
                            </h3>
                            <div>
                                <p className="mb-1">
                                    {jobState.Company}
                                </p>
                            </div>
                            <div className='mb-1'>
                                <img width={20} height={20} className='location-icon' src={locationIcon} alt="location" />
                                <p className='location-text inline ml-2'>{jobState.Location}</p>
                            </div>
                            <div>
                                <p>{jobState.Published}</p>
                            </div>
                            <div className="single-job-type">
                                <div className="badge-wrapper badge-wrapper-full-time">
                                    <div className="badge-fake-wrapper">
                                        <div className='badge  badge-full-time'>
                                            full-time
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col md={3}>
                            {applied ? <div className='badge-applied-wrapper text-center my-2 my-md-0' style={{ width: "140px" }}>
                                <div className='badge-applied-fake-wrapper'>
                                    <div className='badge-applied'>
                                        <p>APPLIED</p>
                                    </div>
                                </div>
                            </div> :
                                <button className="btn btn-original my-2 my-md-0" onClick={Apply}>Apply</button>
                            }
                        </Col>
                        <Col className="my-3">
                            <p>
                                {parse(jobState.Description)}</p>
                        </Col>
                    </Row>
                    }

                    <Col md={12} className="text-center" >
                        {applied ? <div className='badge-applied-wrapper text-center' style={{ width: "140px", margin: "auto" }}>
                            <div className='badge-applied-fake-wrapper'>
                                <div className='badge-applied'>
                                    <p>APPLIED</p>
                                </div>
                            </div>
                        </div> :
                            <button className="btn btn-original" onClick={Apply}>Apply</button>
                        }
                    </Col>
                </Col>
            </Row>
        </Container>
    );
}

export default SingleJob;
